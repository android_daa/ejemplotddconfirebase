package cl.salemlabs.ejemplo2tdd;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends BaseActivity {

    private static final String TAG = "LoginActivity";
    private static final int COD_REGISTRO = 12;
    private EditText etUser;
    private EditText etPass;
    private Button btnAceptar;
    private Button btnRegistrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUser = (EditText) findViewById(R.id.et_user);
        etPass = (EditText) findViewById(R.id.et_pass);
        btnAceptar = (Button) findViewById(R.id.btn_aceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarUsuario();
            }
        });
        btnRegistrar = (Button)findViewById(R.id.btn_registrar);
        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarActividadRegistrar();
            }
        });

    }






    protected boolean isLoginSuccess(String user, String pass) {
        boolean validaUsuario = false;
        if (user.equals("admin") && pass.equals("admin")) {
            validaUsuario = true;
        }
        return validaUsuario;
    }

    protected void validaUsuario(String email, String clave){
        mAuth.signInWithEmailAndPassword(email, clave)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        dismissDialog();
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());
                            mostrarMensajeDeError();
                        }else{
                            cambiarActividad();
                        }
                    }
                });
    }

    private void validarUsuario() {
        showDialog();
        validaUsuario(etUser.getText().toString(), etPass.getText().toString());
    }



    private void mostrarMensajeDeError() {
        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
    }

    private void cambiarActividad() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private void cambiarActividadRegistrar() {
        Intent i = new Intent(this, RegistroActivity.class);
        startActivityForResult(i, COD_REGISTRO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == COD_REGISTRO){
            etUser.setText(data.getStringExtra(RegistroActivity.COD_EMAIL));
        }
    }

    public EditText getEtUser() {
        return etUser;
    }

    public EditText getEtPass() {
        return etPass;
    }

    public Button getBtnAceptar() {
        return btnAceptar;
    }
}
