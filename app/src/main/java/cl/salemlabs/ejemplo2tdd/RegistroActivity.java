package cl.salemlabs.ejemplo2tdd;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class RegistroActivity extends BaseActivity {

    public static final String COD_EMAIL = "email";
    private static final String TAG = "RegistroActivity";
    private EditText etUser;
    private EditText etPass;
    private EditText etRePass;
    private Button btnCrear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        etUser = (EditText) findViewById(R.id.et_user);
        etPass = (EditText) findViewById(R.id.et_pass);
        etRePass = (EditText) findViewById(R.id.et_confirm_pass);
        btnCrear = (Button) findViewById(R.id.btn_crear);
        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearUsuario();
            }
        });
    }

    private void crearUsuario() {
        showDialog();
        if (etUser.getText().toString().length() > 0 &&
                etPass.getText().toString().equals(etRePass.getText().toString())) {
            mAuth.createUserWithEmailAndPassword(etUser.getText().toString(), etPass.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                            dismissDialog();
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Toast.makeText(RegistroActivity.this, R.string.auth_failed,
                                        Toast.LENGTH_SHORT).show();
                            }else{
                                Intent returnIntent = new Intent();
                                returnIntent.putExtra(COD_EMAIL,etUser.getText().toString());
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            }

                        }
                    });
        }
    }
}
